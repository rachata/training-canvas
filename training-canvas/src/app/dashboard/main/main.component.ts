import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
declare const $: any;
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  private valvePosition = {
    "x": 768.2847,
    "y": 257.5645,
    "w": 210.4296,
    "h": 220.036
  }

  private meterPositionRec = {
    "x": 1237,
    "y": 108,
    "w": 139,
    "h": 53
  }


  private meterPosition = {
    "x": 1241.82,
    "y": 150.6
  }


  private count : any = 0;;

  private diagram: any

  private canvasW: any;
  private canvasH: any;

  private context: CanvasRenderingContext2D;

  private statusValve: boolean = false;

  private imgValveOpen: any
  private imgValveClose: any
  

  @ViewChild('diagramMain')
  myCanvas: ElementRef<HTMLCanvasElement>;


  @ViewChild('container')
  container: ElementRef<HTMLElement>


  constructor() { }




  ngOnInit() {


  }


  ngAfterViewInit(): void {


    this.imgValveOpen = new Image();
    this.imgValveOpen.src = "../assets/images/valve-green.png";

    this.imgValveClose = new Image();
    this.imgValveClose.src = "../assets/images/valve-red.png";

    this.myCanvas.nativeElement.addEventListener("mousedown", this.onClickCanvas.bind(this));


    this.drawDiagarm();

  }


  drawDiagarm() {


    this.context = this.myCanvas.nativeElement.getContext('2d');


    this.diagram = new Image();
    this.diagram.src = "../assets/images/diagram-01.png";

    this.diagram.onload = () => {


      var w = this.container.nativeElement.offsetWidth
      var h = (window.innerHeight)

      var n1 = this.getMax(this.getMax(this.diagram.width, this.diagram.height), this.getMax(w, h));
      var n2 = this.getMin(this.getMax(this.diagram.width, this.diagram.height), this.getMax(w, h));

      var ratioW = (n1) / (n2);

      var n3 = this.getMax(
        this.getMin(this.diagram.width, this.diagram.height),
        this.getMin(w, h));
      var n4 = this.getMin(
        this.getMin(this.diagram.width, this.diagram.height),
        this.getMin(w, h));

      var ratioH = (n3) / (n4);

      var imageScale = this.getMax(ratioW, ratioH);

      this.canvasW = this.diagram.width / imageScale;
      this.canvasH = this.diagram.height / imageScale;


      this.myCanvas.nativeElement.width = this.canvasW
      this.myCanvas.nativeElement.height = this.canvasH


      this.context.drawImage(this.diagram, 0, 0, this.canvasW, this.canvasH);


    



    };

  }

  onClickCanvas(ev) {


    var pos = {
      x: (ev.clientX - this.myCanvas.nativeElement.offsetLeft),
      y: (ev.clientY - this.myCanvas.nativeElement.offsetTop)
    }

    var ratioW = this.diagram.width / this.canvasW;
    var ratioH = this.diagram.height / this.canvasH;


    var checkXStart = this.valvePosition["x"] / ratioW;
    var checkYStart = this.valvePosition["y"] / ratioH;

    var checkXEnd = checkXStart + (this.valvePosition["w"] / ratioW);
    var checkYEnd = checkYStart + (this.valvePosition["h"] / ratioH);

    if (pos.x >= checkXStart && pos.x <= checkXEnd && pos.y >= checkYStart && pos.y <= checkYEnd) {
 
      if(this.statusValve == false){
        this.count = this.count+5;

        this.addText(this.count);
        this.valveOpen();

      }else{
        this.valveClose();
      }
      this.statusValve = !this.statusValve;


     

    }


  }

  valveOpen() {

    var x = (this.valvePosition.x / (this.diagram.width / this.canvasW));
    var y = (this.valvePosition.y / (this.diagram.height / this.canvasH));

    var w = (this.valvePosition.w / (this.diagram.width / this.canvasW));
    var h = (this.valvePosition.h / (this.diagram.height / this.canvasH));


    this.context.drawImage(this.imgValveOpen, x, y, w, h);


  }

  valveClose() {

    var x = (this.valvePosition.x / (this.diagram.width / this.canvasW));
    var y = (this.valvePosition.y / (this.diagram.height / this.canvasH));

    var w = (this.valvePosition.w / (this.diagram.width / this.canvasW));
    var h = (this.valvePosition.h / (this.diagram.height / this.canvasH));


    this.context.drawImage(this.imgValveClose, x, y, w, h);


  }

  addText(count : Number) {
    var xFlowRate = (this.meterPosition.x / (this.diagram.width / this.canvasW));
    var yFlowRate = (this.meterPosition.y / (this.diagram.height / this.canvasH));

    var x = (this.meterPositionRec.x / (this.diagram.width / this.canvasW));
    var y = (this.meterPositionRec.y / (this.diagram.height / this.canvasH));

    var w = (this.meterPositionRec.w / (this.diagram.width / this.canvasW));
    var h = (this.meterPositionRec.h / (this.diagram.height / this.canvasH));


    this.context.fillStyle = "#FFF";
    this.context.fillRect(x , y  , w , h);



    this.context.fillStyle = "#FF0000";

    this.context.font = "25px Prompt";
    this.context.fillText(this.count.toString() + " L", xFlowRate, yFlowRate);

  }


  getMax(n1, n2) {
    if (n1 >= n2) {
      return n1;
    } else {
      return n2;
    }
  }
  getMin(n1, n2) {
    if (n1 <= n2) {
      return n1;
    } else {
      return n2;
    }
  }


}
